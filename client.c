#include<stdio.h> //for printf
#include<string.h> //memset
#include<linux/types.h>
#include<sys/socket.h>    //for socket ofcourse
#include<stdlib.h> //for exit(0);
#include<errno.h> //For errno - the error number
#include<netinet/udp.h>   //Provides declarations for udp header
#include<netinet/ip.h>    //Provides declarations for ip header
#include<inttypes.h>
#include<sys/ioctl.h>
#include<linux/if.h>
#include<net/ethernet.h>

#define DGRAM_SIZE 4096

uint16_t checksum(uint16_t *addr, int len) {
    int count = len;
    register uint32_t sum = 0;
    uint16_t answer = 0;
    // Sum up 2-byte values until none or only one byte left.
    while (count > 1) {
        sum += *(addr++);
        count -= 2;
    }
    // Add left-over byte, if any.
    if (count > 0) {
        sum += *(uint8_t *) addr;
    }
    // Fold 32-bit sum into 16 bits; we lose information by doing this,
    // increasing the chances of a collision.
    // sum = (lower 16 bits) + (upper 16 bits shifted right 16 bits)
    while (sum >> 16) {
        sum = (sum & 0xffff) + (sum >> 16);
    }
    // Checksum is one's compliment of sum.
    answer = ~sum;
    return (answer);
}

uint16_t udp4_checksum(struct iphdr iphdr, struct udphdr udphdr, uint8_t *payload, int payloadlen) {
    char buf[IP_MAXPACKET];
    char *ptr;
    int chksumlen = 0;
    int i;

    ptr = &buf[0]; // ptr points to beginning of buffer buf

    // Copy source IP address into buf (32 bits)
    memcpy(ptr, &iphdr.saddr, sizeof (iphdr.saddr));
    ptr += sizeof (iphdr.saddr);
    chksumlen += sizeof (iphdr.saddr);
    // Copy destination IP address into buf (32 bits)
    memcpy(ptr, &iphdr.daddr, sizeof (iphdr.daddr));
    ptr += sizeof (iphdr.daddr);
    chksumlen += sizeof (iphdr.daddr);
    // Copy zero field to buf (8 bits)
    *ptr = 0;
    ptr++;
    chksumlen += 1;
    // Copy transport layer protocol to buf (8 bits)
    memcpy(ptr, &iphdr.protocol, sizeof (iphdr.protocol));
    ptr += sizeof (iphdr.protocol);
    chksumlen += sizeof (iphdr.protocol);
    // Copy UDP length to buf (16 bits)
    memcpy(ptr, &udphdr.len, sizeof (udphdr.len));
    ptr += sizeof (udphdr.len);
    chksumlen += sizeof (udphdr.len);
    // Copy UDP source port to buf (16 bits)
    memcpy(ptr, &udphdr.source, sizeof (udphdr.source));
    ptr += sizeof (udphdr.source);
    chksumlen += sizeof (udphdr.source);
    // Copy UDP destination port to buf (16 bits)
    memcpy(ptr, &udphdr.dest, sizeof (udphdr.dest));
    ptr += sizeof (udphdr.dest);
    chksumlen += sizeof (udphdr.dest);
    // Copy UDP length again to buf (16 bits)
    memcpy(ptr, &udphdr.len, sizeof (udphdr.len));
    ptr += sizeof (udphdr.len);
    chksumlen += sizeof (udphdr.len);
    // Copy UDP checksum to buf (16 bits)
    // Zero, since we don't know it yet
    *ptr = 0;
    ptr++;
    *ptr = 0;
    ptr++;
    chksumlen += 2;
    // Copy payload to buf
    memcpy(ptr, payload, payloadlen);
    ptr += payloadlen;
    chksumlen += payloadlen;
    // Pad to the next 16-bit boundary
    for (i = 0; i < payloadlen % 2; i++, ptr++) {
        *ptr = 0;
        ptr++;
        chksumlen++;
    }
    return checksum((uint16_t *) buf, chksumlen);
}

int main(int argc, char **argv) {
    //Create a raw socket of type 
    int s = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW);
    //int s = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
    //int s = socket(AF_INET, SOCK_RAW, IPPROTO_UDP);
    if (s == -1) {
        //socket creation failed, may be because of non-root privileges
        perror("Failed to create raw socket");
        exit(1);
    }
    //Datagram to represent the packet
    char datagram[DGRAM_SIZE], source_ip[32], *data, *pseudogram;
    //zero out the packet buffer
    memset(datagram, 0, DGRAM_SIZE);

    struct ifreq ifreq_i;
    memset(&ifreq_i, 0, sizeof (ifreq_i));
    strncpy(ifreq_i.ifr_ifrn.ifrn_name, "lo", IFNAMSIZ - 1);
    if ((ioctl(s, SIOCGIFINDEX, &ifreq_i)) < 0) {
        perror("ioctl");
        exit(1);
    }
    printf("index = %d\n", ifreq_i.ifr_ifru.ifru_ivalue);

    struct ifreq ifreq_c;
    memset(&ifreq_c, 0, sizeof (ifreq_c));
    strncpy(ifreq_c.ifr_ifrn.ifrn_name, "lo", IFNAMSIZ - 1); //giving name of Interface

    if ((ioctl(s, SIOCGIFHWADDR, &ifreq_c)) < 0) //getting MAC Address
    {
        perror("ioctl");
        exit(1);
    }

    struct ifreq ifreq_ip;
    memset(&ifreq_ip, 0, sizeof (ifreq_ip));
    strncpy(ifreq_ip.ifr_ifrn.ifrn_name, "lo", IFNAMSIZ - 1); //giving name of Interface
    if (ioctl(s, SIOCGIFADDR, &ifreq_ip) < 0) //getting IP Address
    {
        perror("ioctl");
        exit(1);
    }

    struct ethhdr *eth = (struct ethhdr *) datagram;

    eth->h_source[0] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[0]);
    eth->h_source[1] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[1]);
    eth->h_source[2] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[2]);
    eth->h_source[3] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[3]);
    eth->h_source[4] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[4]);
    eth->h_source[5] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[5]);

    eth->h_dest[0] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[0]);
    eth->h_dest[1] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[1]);
    eth->h_dest[2] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[2]);
    eth->h_dest[3] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[3]);
    eth->h_dest[4] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[4]);
    eth->h_dest[5] = (unsigned char) (ifreq_c.ifr_ifru.ifru_hwaddr.sa_data[5]);

    eth->h_proto = htons(ETH_P_IP);

    //IP header
    struct iphdr *iph = (struct iphdr *) (datagram + sizeof (struct ethhdr));
    //struct iphdr *iph = (struct iphdr *) datagram;
    //UDP header
    struct udphdr *udph = (struct udphdr *) (datagram + sizeof (struct iphdr) + sizeof (struct ethhdr));
    //struct udphdr *udph = (struct udphdr *) (datagram + sizeof (struct iphdr));
    //struct udphdr *udph = (struct udphdr *) (datagram);

    struct sockaddr_in sin;
    //Data part
    data = datagram + sizeof(struct ethhdr) + sizeof (struct iphdr) + sizeof (struct udphdr);
    //data = datagram + sizeof (struct iphdr) + sizeof (struct udphdr);
    //data = datagram + sizeof (struct udphdr);
    strcpy(data, "QWERTYPASDFGHZXCVBNMMNBVCXZLKJHGFDSAPOIUYTREWQ\0");
    //some address resolution
    strcpy(source_ip, "127.0.0.1");

    sin.sin_family = AF_INET;
    sin.sin_port = htons(6666);
    sin.sin_addr.s_addr = inet_addr(source_ip);
    //Fill in the IP Header
    iph->ihl = 5;
    iph->version = 4;
    iph->tos = 0;
    iph->tot_len = sizeof(struct iphdr) + sizeof(struct udphdr);
    iph->id = htonl(54321); //Id of this packet
    iph->frag_off = 0;
    iph->ttl = 64;
    iph->protocol = IPPROTO_UDP;
    iph->check = 0; //Set to 0 before calculating checksum
    iph->saddr = inet_addr(source_ip); //Spoof the source ip address
    iph->daddr = sin.sin_addr.s_addr;
    //Ip checksum
    iph->check = checksum((unsigned short *) &iph, iph->tot_len >> 1);
    //iph->check = csum((unsigned short *) datagram, iph->tot_len);
    printf("IP checksum - 0x%x\n", htons(iph->check));
    //UDP header
    udph->source = htons(6666);
    udph->dest = htons(8888);
    udph->len = htons(sizeof (struct udphdr) +strlen(data)); //tcp header size
    udph->check = 0; //leave checksum 0 now, filled later by pseudo header
    udph->check = udp4_checksum(*iph, *udph, data, strlen(data));
    printf("UDP checksum - 0x%x\n", htons(udph->check));
    //udph->check = csum((unsigned short*) pseudogram, psize);

    //loop if you want to flood :)
    //while (1)
    {
        //Send the packet
        //if (sendto(s, datagram, 8 + strlen(data), 0, (struct sockaddr *) &sin, (socklen_t)sizeof (sin)) < 0) {
        if (sendto(s, datagram, iph->tot_len, 0, (struct sockaddr *) &sin, sizeof (sin)) < 0) {
            perror("sendto failed");
        }//Data send successfully
        else {
            printf("Packet Send. Length : %d \n", iph->tot_len);
            //printf("Packet Send. Length : %d \n", sizeof(struct udphdr) + strlen(data));
            printf("%s\n", data);
        }
    }

    return 0;
}
